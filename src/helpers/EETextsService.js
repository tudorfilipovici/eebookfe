var gTexts = {};

export function setTexts(lang, texts) {
  gTexts[lang] = texts;
}

export function getText(lang, screenId, textId) {
  if (!gTexts[lang] || !gTexts[lang][screenId] || !gTexts[lang][screenId][textId]) {
    return `MISSING_TEXT:${lang}/${screenId}/${textId}`;
  }

  return gTexts[lang][screenId][textId];
}
