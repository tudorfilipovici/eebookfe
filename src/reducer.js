import { combineReducers } from 'redux'
import Layout from './reducers/Layout'
import Search from './reducers/Search'
import Availability from './reducers/Availability'
import Data from './reducers/Data'
import Texts from './reducers/Texts'

export default combineReducers({
  Data,
  Search,
  Availability,
  Layout,
  Texts
});
