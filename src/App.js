import React, {Component} from 'react'
import { Provider } from 'react-redux'
// import { LOCATION_CHANGE, syncHistoryWithStore } from 'react-router-redux'
import { Router, Route, browserHistory } from 'react-router'
import Search from './components/pages/search'
import Availability from './components/pages/availability'
import Layout from './components/layout'
import Store  from './store'

import {setLang} from './reducers/Texts'

const store = Store.create();
store.dispatch(setLang('en'));

export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route component={Layout}>
            <Route key="/" path="/" component={Search}/>
          </Route>
          <Route key="availability" path="/availability" component={Availability}/>
        </Router>
      </Provider>
    );
  }
};
