import axios from 'axios'
import config from '../json/config'

const initialSearch = {
  routes: [],
  airports: {},
  countries: {},
  error:  false,
  passengers: {},
  dates:  {}
};

/**
 * Search data
 * @param {Object} state  The global state object
 * @param {Object} action The requested action
 */
export default function Data(state = initialSearch, action) {
  switch (action.type) {
    case 'ROUTES_PENDING':
      return {
        ...state,
        error:  null,
        routes: initialSearch.routes,
        passengers: initialSearch.passengers,
        dates: initialSearch.dates
      };

    case 'ROUTES_FULFILLED':
      return {
        ...state,
        ...action.payload.data
      };

    case 'ROUTES_REJECTED':
      return {
        ...state,
        ...initialSearch,
        error:  true
      };

    default:
      return state;
  }
}

export function retrieveRoutes() {
  if (config.isOffline) {
    return {
      type: 'ROUTES_FULFILLED',
      payload: {data: require('../json/destinations.json') }
    }
  }

  return {
    type:    'ROUTES',
    payload: axios.get(config.server + "/routes?lang=en")
  };
}
