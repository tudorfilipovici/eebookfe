import axios from 'axios'
import config from '../json/config'
import {setTexts} from '../helpers/EETextsService'

const initialSearch = {
  lang:      "",
  languages: []
};

/**
 * Search data
 * @param {Object} state  The global state object
 * @param {Object} action The requested action
 */
export default function Texts(state = initialSearch, action) {
  switch (action.type) {
    // case 'TEXTS_PENDING':
    //   return {
    //     ...state,
    //     error:  null,
    //     ...initialSearch
    //   };

    // case 'TEXTS_FULFILLED':
    //   return {
    //     ...state,
    //     ...action.payload.data
    //   };

    // case 'TEXTS_REJECTED':
    //   return {
    //     ...state,
    //     ...initialSearch,
    //     error:  true
    //   };

    case 'SET_LANG_FULFILLED':
      return {
        ...state,
        lang: action.payload
      };

    case 'SET_LANGUAGES':
      return {
        ...state,
        languages: action.payload
      };

    default:
      return state;
  }
}

// export function setLang(lang="en" ){
//   return {
//     type: 'SET_LANG',
//     payload: lang
//   }
// }

export function setLanguages(languages){
  return {
    type: 'SET_LANGUAGES',
    payload: languages
  }
}

export function setLang(lang) {
  if (config.isOffline) {
      setTexts(lang, require('../json/texts.' + lang + '.json').texts);
      return {
        type: 'SET_LANG_FULFILLED',
        payload: lang
      };
  } 

  let request = axios.get(config.server + `/texts?functions=fb_input&lang=${lang}`)
    .done(function (response) {
      setTexts(lang, response.data.texts)
    });

  return {
    type: 'SET_LANG',
    payload: request
  };
}

/**
* Not used i keep it here in case we want to save the texts in redux
*/
// export function getAllTexts(lang="en") {
//   if (config.isOffline) {
//     return {
//       type: 'TEXTS_FULFILLED',
//       payload: {data: require('../json/texts.json') }
//     }
//   }
//
//   return {
//     type:    'TEXTS',
//     payload: axios.get(config.server + `/texts?functions=error,fb_input&lang=${lang}`)
//   };
// }
