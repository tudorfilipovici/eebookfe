import config from '../json/config'
import { browserHistory } from 'react-router'

const NOT_READY = 'NOT_READY';
const LOADING   = 'LOADING';
const ERROR     = 'ERROR';
const READY     = 'READY';

const initialState = {
  state: NOT_READY,
};

/**
 * Availability data
 * @param {Object} state  The global state object
 * @param {Object} action The requested action
 */
export default function Availability(state = initialState, action) {
  switch (action.type) {
    case 'AVAILABILITY_PENDING':
      return {
        ...state,
        state: LOADING
      };

    case 'AVAILABILITY_REJECTED':
      return {
        ...state,
        state: ERROR
      };

    case 'AVAILABILITY_FULFILLED':
      return {
        state: READY,
        ...action.payload.data
      };

    default:
      return state;
  }
}

export function requestAvailability(options) {
  if (config.isOffline) {
    browserHistory.push("/availability");
    return {
      type: 'AVAILABILITY_FULFILLED',
      payload: {data: require('../json/availability.json') }
    }
  }

  let promise = axios.post(config.server + "/routes?lang=en", options)
    .then(resp => {
      browserHistory.push("/availability");
    });

  return {
    type:    'AVAILABILITY',
    payload: promise
  };
}