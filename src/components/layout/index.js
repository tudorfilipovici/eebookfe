import './Layout.scss'
import ExtraMenu from '../layout/ExtraMenu'
import LeftSection from '../layout/LeftSection'
import {getText} from '../../helpers/EETextsService'
import {setLang} from '../../reducers/Texts'

import React, { Component } from 'react'
import { connect } from 'react-redux'

function AppLayout({ children, lang, menu, leftSection, setEnglish, setRomanian }) {
  let cls = ["mp-pusher"];
  menu && cls.push("mp-pushed");

  return (
    <div className="container-fluid">

      <ul className="nav nav-pills">
        <li role="presentation">
          <a href="#en" onClick={setEnglish}>{getText(lang,'layout','lang_label_en')}</a>
        </li>
        <li role="presentation">
          <a href="#ro" onClick={setRomanian}>{getText(lang,'layout','lang_label_ro')}</a>
        </li>
      </ul>

      <div className={cls.join(" ")}>

        <div className="row-fluid scroller">
          <div className="hidden-xs col-sm-7 col-md-8 col-lg-9">
            <LeftSection lang={lang} component={leftSection}/>
          </div>
          <div className="col-xs-12 col-sm-5 col-md-4 col-lg-3">
            {children}
          </div>
        </div>

        <aside className="mp-menu">
          <ExtraMenu component={menu} lang={lang}/>
        </aside>

      </div>
    </div>
  );
}

function getProperties(state) {
  return {
    lang: state.Texts.lang,
    menu: state.Layout.menu || null,
    leftSection:  state.Layout.leftSection  || null
  }
}

function getActions(dispatch) {
  return {
    setEnglish:  () => dispatch(setLang('en')),
    setRomanian: () => dispatch(setLang('ro')),
  }
}

export default connect(getProperties, getActions)(AppLayout);
