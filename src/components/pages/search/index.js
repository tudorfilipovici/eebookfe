import './Search.scss'

import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { browserHistory } from 'react-router'
import Button               from '../../common/Button'
import AirportSearchInput   from './sub/AirportSearchInput'
import DatePicker           from './sub/DatePicker'
import PaxNumSpinner        from './sub/PaxNumSpinner'
import {getText} from '../../../helpers/EETextsService'

import { map, uniqBy, filter } from 'lodash'
import {
  selectOrigin,
  selectDestin,
  toggleReturn,
  selectReturnDate,
  setPaxNum,
  resetSearch,
} from '../../../reducers/Search'

import {
  requestAvailability
} from '../../../reducers/Availability'

import {retrieveRoutes} from '../../../reducers/Data';

// import Dropdown from '../../common/Dropdown'

class Form extends Component {

  componentDidMount() {
    if (!this.props.options || this.props.options.length < 1) {
      this.props.dispatch(retrieveRoutes());
    }
  }

  onChangeDate( dateSelector) {
    dateSelector = this.props.menu ? null : dateSelector;
    this.props.dispatch({ type: 'OPEN_SIDE_MENU', payload: dateSelector });
  }

  onOriginSelected(itm) {
    this.props.dispatch(selectOrigin(itm.value, itm.label));
  }

  onDestinSelected(itm) {
    this.props.dispatch(selectDestin(itm.value, itm.label));
  }

  onToggleReturn() {
    this.props.dispatch(selectReturnDate(null));
    this.props.dispatch(toggleReturn());
  }

  getOrigins() {
    let options = this.props.options;

    if (this.props.destinCode !== "") {
      options = filter(options, {destinationCode: this.props.destinCode});
    }

    let origins = map(options, (itm) => {
      return {
        label: this.props.airports[itm.originCode].name,
        value: itm.originCode
      };
    });

    origins = uniqBy(origins, 'value');
    return origins;
  }

  getDestinations() {
    let options = this.props.options;

    if (this.props.originCode !== "") {
      options = filter(options, {originCode: this.props.originCode});
    }

    let destins = map(options, itm => {
      return {
        label: this.props.airports[itm.destinationCode].name,
        value: itm.destinationCode
      }
    });

    destins = uniqBy(destins, 'value');
    return destins;
  }

  onPaxNumberChange( paxType, operation){
    this.props.dispatch(setPaxNum(paxType, operation));
  }

  clearOrigin() {
    this.props.dispatch(selectOrigin(""));
  }

  clearDestin() {
    this.props.dispatch(selectDestin(""));
  }

  onResetSearch(){
    this.props.dispatch(resetSearch(""));
  }

  onSearch() {
    this.props.dispatch(requestAvailability({
      origin:    this.props.originCode,
      destin:    this.props.destinCode,
      departure: this.props.departureDate,
      return:    this.props.returnDate,
      numAdt:    this.props.numAdt,
      numChd:    this.props.numChd,
      numInf:    this.props.numInf,
    }));
  }

  render() {
    let lang = this.props.lang;
    return (
      <form className={"search " + this.props.className}>
        <h3>{getText(lang,'fb_input','title')}</h3>

        <fieldset>
          <legend className="sr-only">Airports</legend>
          <AirportSearchInput
            label={getText(lang,'fb_input','origin_label')}
            error={this.props.error}
            onInput={this.clearOrigin.bind(this)}
            onOptionSelected={this.onOriginSelected.bind(this)}
            value={this.props.originName}
            options={this.getOrigins()}
          />
          <AirportSearchInput
            label={getText(lang,'fb_input','destination_label')}
            error={this.props.error}
            onInput={this.clearDestin.bind(this)}
            onOptionSelected={this.onDestinSelected.bind(this)}
            value={this.props.destinName}
            options={this.getDestinations()}
          />
        </fieldset>

        <fieldset className="row dates compact">
          <legend className="sr-only">Dates</legend>
          <div  className="col-xs-6">
            <DatePicker label={getText(lang,'fb_input','out_date_label')}
              onChangeDate={this.onChangeDate.bind(this, 'departureDate')}
              date={this.props.departureDate}
              />
          </div>
          <div  className="col-xs-6">
            <DatePicker
              label={getText(lang,'fb_input','in_date_label')}
              disabled={this.props.isOneWay}
              toggleDisabled={this.onToggleReturn.bind(this)}
              date={this.props.returnDate}
              onChangeDate={this.onChangeDate.bind(this, 'returnDate')}
            />
          </div>
        </fieldset>

        <fieldset className="row paxes compact">
          <legend className="sr-only">Passengers</legend>
          <div  className="col-xs-4">
            <PaxNumSpinner 
              label="Adults" 
              paxType="numAdt" 
              minNum={1} 
              maxNum={this.props.passengers.maxAdults} 
              from={12} 
              unit="years" 
              num={this.props.numAdt} 
              onPaxNumberChange={this.onPaxNumberChange.bind(this)}/>
          </div>
          <div  className="col-xs-4">
            <PaxNumSpinner 
              label="Children" 
              paxType="numChd" 
              minNum={0} 
              maxNum={this.props.passengers.maxChildren} 
              from={2} 
              to={12} 
              unit="years" 
              num={this.props.numChd} 
              onPaxNumberChange={this.onPaxNumberChange.bind(this)}/>
          </div>
          <div  className="col-xs-4">
            <PaxNumSpinner 
              label="Infants" 
              paxType="numInf" 
              minNum={0} 
              maxNum={this.props.passengers.maxInfants} 
              from={0} 
              to={23} 
              unit="months" 
              num={this.props.numInf} 
              onPaxNumberChange={this.onPaxNumberChange.bind(this)}/>
          </div>
        </fieldset>

        <fieldset className="row actions compact">
          <legend className="sr-only">Actions</legend>
          <div  className="col-xs-6">
            <Button className="default block" onClick={this.onResetSearch.bind(this)}>
              <i className="glyphicon glyphicon-repeat"></i>
              <span className="btn--text">Reset</span>
            </Button>
          </div>
          <div  className="col-xs-6">
            <Button className="primary block" onClick={this.onSearch.bind(this)}>
              <i className="glyphicon glyphicon-search"></i>
              <span className="btn--text">Search</span>
            </Button>
          </div>
        </fieldset>
      </form>
    );
  }
}

Form.defaultProps = {
  className: ""
};

function getProperties(state) {
  const srch  = state.Search;
  const data  = state.Data;
  // const avail = state.Availability;
  return {
    lang:       state.Texts.lang,
    originCode: srch.originCode,
    destinCode: srch.destinCode,
    originName: srch.originName,
    destinName: srch.destinName,
    options:    data.routes,
    airports:    data.airports,
    countries:    data.countries,
    passengers:    data.passengers,
    dates:      data.dates,
    error:      srch.error,
    isOneWay:   srch.numJourneys < 2,
    departureDate: srch.departureDate,
    returnDate: srch.returnDate,
    numAdt: srch.numAdt,
    numChd: srch.numChd,
    numInf: srch.numInf,
    menu: state.Layout.menu
  };
}

export default connect(getProperties)(Form);
