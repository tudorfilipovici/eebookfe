import React, { Component } from 'react'
import { connect } from 'react-redux'
import {getText} from '../../../helpers/EETextsService'


class Availability extends Component {
	renderTabs(tabs) {
		return (
			<ul className="nav nav-tabs"> {
				tabs.map(tab => {
					const isActive = tab.active ? 'active' : '';
					return (
						<li role="presentation" key={tab.date}>
							<a href="#" className={isActive}>
								<span>{tab.date}</span>
								<br />
								<small>{tab.currency ? tab.amount : tab.info}</small>
							</a>
						</li>
					);
				})
			}</ul>
		);
	}

	renderFlights(items) {
		return (
			<div className="clearfix">{
				items.map(item => {
					const flight 	 = this.props.flightsById[item.id];
					const numStops = flight.legs.length;
					let stops;
					switch (numStops) {
						case 1:
							stops = 'Non-Stop';
							break;
						case 2:
							stops = `${numStops} Stop`;
							break;
						default:
							stops = `${numStops} Stops`;
							break;
					}
					return (
						<div className="row-fluid" key={item.id}>
							<div className="col-xs-2 bg-success">
								Economy
							</div>
							<div className="col-xs-2 bg-primary">
								Business
							</div>
							<div className="col-xs-2">
								{stops}
							</div>
							<div className="col-xs-2">
								{parseInt(flight.duration / 60, 10)}:{parseInt(flight.duration % 60, 10)}
							</div>
							<div className="col-xs-2">
								{flight.originCode}<br/>
								{flight.departureTime}
							</div>
							<div className="col-xs-2">
								{flight.destinCode}<br/>
								{flight.arrivalTime}
							</div>
						</div>
					);
				})
			}</div>
		);
	}

	render() {
		return (
			<section>
				<h2>Outbounds</h2>
				{this.renderTabs(this.props.tabs.outbounds)}
				{this.renderFlights(this.props.initial.outbounds)}
				<hr/>
				<h2>Inbounds</h2>
				{this.renderTabs(this.props.tabs.inbounds)}
				{this.renderFlights(this.props.initial.inbounds)}
			</section>
		);
	}
}

function getProperties(state) {
	return {
		...state.Availability,
		lang: state.Texts.lang
	};
}

export default connect(getProperties)(Availability);
