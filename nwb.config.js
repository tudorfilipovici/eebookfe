module.exports = {
  type: 'react-app',
  webpack: {
    extra: {
      devtool: 'source-map' //TODO modify for live
    },
    loaders: {
      'vendor-sass-css': {
        modules: true,
        localIdentName: '[hash:base64:5]'
      }
    }
  }
}
